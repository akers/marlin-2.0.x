# Marlin 2.0.x修改版固件
## 配置修改
### 选择主板类型
Configuration.h 找到`define MOTHERBOARD BOARD_RAMPS_14_EFB`
修改为：
```
// 2.x 主板
#ifndef MOTHERBOARD
#define MOTHERBOARD BOARD_MKS_GEN_L_V2
#endif

// 1.0 主板
#ifndef MOTHERBOARD
#define MOTHERBOARD BOARD_MKS_GEN_L
#endif
```

### 打印平台平台尺寸125 110 160
Configuration.h，定位X_BED_SIZE，可配置打印尺寸
小树T3不改Y轴的话，X、Y滑车实际行程是125、110，可酌情调整，注意改动固件后需同步修改cura中的配置
```
// The size of the printable area
#define X_BED_SIZE 125 // 打印平台X轴大小
#define Y_BED_SIZE 110 // 打印平台Y轴大小

// Travel limits (mm) after homing, corresponding to endstop positions.
#define X_MIN_POS 0
#define Y_MIN_POS 0
#define Z_MIN_POS 0
#define X_MAX_POS X_BED_SIZE
#define Y_MAX_POS Y_BED_SIZE
#define Z_MAX_POS 160 // 打印最大高度（Z轴行程）
```

### 修改屏幕配置
我的测试机器小树T3配的KMS MINI12864屏幕，大屏没测过

#### 12864大屏
修改Configuration.h
放开`//#define REPRAP_DISCOUNT_FULL_GRAPHIC_SMART_CONTROLLER`行的注释
```
//
// RepRapDiscount FULL GRAPHIC Smart Controller
// https://reprap.org/wiki/RepRapDiscount_Full_Graphic_Smart_Controller
//
#define REPRAP_DISCOUNT_FULL_GRAPHIC_SMART_CONTROLLER
```

#### MINI12864屏幕
修改Configuration.h
放开`//#define MINIPANEL`行的注释
```
//
// MakerLab Mini Panel with graphic
// controller and SD support - https://reprap.org/wiki/Mini_panel
//
#define MINIPANEL
```
放开`//#define MKS_MINI_12864`行注释
```
//
// MKS MINI12864 with graphic controller and SD support
// https://reprap.org/wiki/MKS_MINI_12864
//
#define MKS_MINI_12864
```
#### 打开SD卡支持
放开`//#define SDSUPPORT`行注释
```
/**
 * SD CARD
 *
 * SD Card support is disabled by default. If your controller has an SD slot,
 * you must uncomment the following option or it won't work.
 */
#define SDSUPPORT
```

#### 开启中文
Configuration.h 修改`LCD_LANGUAGE`的值为zh_CN
```
// #define LCD_LANGUAGE en
#define LCD_LANGUAGE zh_CN
```

#### 长文件名滚动显示
修改Configuration_adv.h，放开`SCROLL_LONG_FILENAMES`行的注释
```
// Enable this option to scroll long filenames in the SD card menu
#define SCROLL_LONG_FILENAMES
```

#### 独立轴复位菜单
显示独立的X、Y、Z轴复位菜单，方便调试
修改Configuration.h，放开`#define INDIVIDUAL_AXIS_HOMING_MENU`注释
```
//
// Individual Axis Homing
//
// Add individual axis homing items (Home X, Home Y, and Home Z) to the LCD menu.
//
#define INDIVIDUAL_AXIS_HOMING_MENU
```

#### 打开LCD对EEPROM读写功能
放开以下注释
```
// #define EEPROM_SETTINGS
```

#### 旋钮方向
如果刷机后旋钮方向不对改这两个
```
// 数值方向，默认正向，放开注释反向
#define REVERSE_ENCODER_DIRECTION
// 菜单方向，默认正向，放开注释反向
#define REVERSE_MENU_DIRECTION
```

### 喷头
#### 喷头温度上限
小树T3用的MK8 0.5mm喷头，建议限制250度
```
#define HEATER_0_MAXTEMP 250
```
#### 预加热
配置预加热时的喷头目标温度
修改Configurtion.h，定位PREHEAT_1_LABEL
```
#define PREHEAT_1_LABEL "PLA"
#define PREHEAT_1_TEMP_HOTEND 195 // PLA材料喷头预热温度
#define PREHEAT_1_TEMP_BED 60     // PLA材料热床预热温度
#define PREHEAT_1_TEMP_CHAMBER 35
#define PREHEAT_1_FAN_SPEED 0 // Value from 0 to 255

#define PREHEAT_2_LABEL "ABS"
#define PREHEAT_2_TEMP_HOTEND 260 // ABS材料喷头预热温度
#define PREHEAT_2_TEMP_BED 80     // ABS材料热床预热温度（注意打印平台允许的最高温度！！）
#define PREHEAT_2_TEMP_CHAMBER 35
#define PREHEAT_2_FAN_SPEED 0 // Value from 0 to 255
```

### 热床
#### 开启热床
```
#define TEMP_SENSOR_BED 1
```
#### 热床温度上限
```
#define BED_MAXTEMP 100
```

### 断料开关
放开Configuration.h`//#define NOZZLE_PARK_FEATURE`、`//#define FILAMENT_RUNOUT_SENSOR`注释开启断料开关支持
```
#define FILAMENT_RUNOUT_SENSOR
#define NOZZLE_PARK_FEATURE
```
放开Configuration_adv.h`//#define ADVANCED_PAUSE_FEATURE`的注释
```
#define ADVANCED_PAUSE_FEATURE
#if ENABLED(ADVANCED_PAUSE_FEATURE)
```

配置断料检测信号
```
// 断料开关断料时的信号，默认是低电平（机械开关），如果断料检测无效，把LOW改为HIGH
#define FIL_RUNOUT_STATE LOW // Pin state indicating that filament is NOT present.
```

将Z_MAX限位开关引脚定义为断料检测
修改pins_RAMPS.h
去掉Z_MAX_PIN后面的数值
```
  #ifndef Z_MAX_PIN
    #define Z_MAX_PIN                         
  #endif
```
FIL_RUNOUT_PIN 值改为19
```
// RAMPS 1.4 DIO 4 on the servos connector
#ifndef FIL_RUNOUT_PIN                        
  // #define FIL_RUNOUT_PIN                       4
  #define FIL_RUNOUT_PIN                      19
#endif
```

### 步进电机
#### 电机步进配置
Configuration.h，修改DEFAULT_AXIS_STEPS_PER_UNIT
小树T3默认是80, 80, 400, 90，如果做了精调就把精调后的数字填入
```
#define DEFAULT_AXIS_STEPS_PER_UNIT \
  {                                 \
    82.05, 82.05, 408.16, 90        \
  }
```

#### XYZ轴使用TMC2209
Configuration.h 注释以下几行：
```
// #define X_DRIVER_TYPE  A4988
// #define Y_DRIVER_TYPE  A4988
// #define Z_DRIVER_TYPE  A4988
```
添加以下几行：
```
// XYZ都使用TMC2209 uart
#define X_DRIVER_TYPE TMC2209
#define Y_DRIVER_TYPE TMC2209
#define Z_DRIVER_TYPE TMC2209
```

### 驱动电流调整
配置代码在 Configuration_adv.h
搜HAS_TRINAMIC_CONFIG定位，相关配置说明：
**HOLD_MULTIPLIER：**配置静止时的智能降流，默认是降低至50%电流
**X_CURRENT：**配置X轴电机电流，单位是毫安，峰值电流 = 配置电流 * 1.414，小树的T3比较小巧，800mA以下都够用了如果滑车调的比较顺滑的话可以用600到700，比较安静，具体情况要看自己机器调整
**Y_CURRENT：**配置X轴电机电流，单位是毫安
**Z_CURRENT：**Z轴电机电流可以适当大一些，因为Z轴负载一般比价高一点
**X_MICROSTEPS、Y_MICROSTEPS、Z_MICROSTEPS：**调整X、Y、Z轴的细分，一般16就可以，调了细分要同时调整脉冲
```
#if AXIS_IS_TMC(X)
#define X_CURRENT 680            // (mA) RMS current. Multiply by 1.414 for peak current.
#define X_CURRENT_HOME X_CURRENT // (mA) RMS current for sensorless homing
#define X_MICROSTEPS 16          // 0..256
#define X_RSENSE 0.11
#define X_CHAIN_POS -1 // -1..0: Not chained. 1: MCU MOSI connected. 2: Next in chain, ...
//#define X_INTERPOLATE  true      // Enable to override 'INTERPOLATE' for the X axis
#endif

#if AXIS_IS_TMC(Y)
#define Y_CURRENT 680
#define Y_CURRENT_HOME Y_CURRENT
#define Y_MICROSTEPS 16
#define Y_RSENSE 0.11
#define Y_CHAIN_POS -1
//#define Y_INTERPOLATE  true
#endif

#if AXIS_IS_TMC(Z)
#define Z_CURRENT 750
#define Z_CURRENT_HOME Z_CURRENT
#define Z_MICROSTEPS 16
#define Z_RSENSE 0.11
#define Z_CHAIN_POS -1
//#define Z_INTERPOLATE  true
#endif
```


# Marlin 3D Printer Firmware

![GitHub](https://img.shields.io/github/license/marlinfirmware/marlin.svg)
![GitHub contributors](https://img.shields.io/github/contributors/marlinfirmware/marlin.svg)
![GitHub Release Date](https://img.shields.io/github/release-date/marlinfirmware/marlin.svg)
[![Build Status](https://github.com/MarlinFirmware/Marlin/workflows/CI/badge.svg?branch=bugfix-2.0.x)](https://github.com/MarlinFirmware/Marlin/actions)

<img align="right" width=175 src="buildroot/share/pixmaps/logo/marlin-250.png" />

Additional documentation can be found at the [Marlin Home Page](https://marlinfw.org/).
Please test this firmware and let us know if it misbehaves in any way. Volunteers are standing by!

## Marlin 2.0

Marlin 2.0 takes this popular RepRap firmware to the next level by adding support for much faster 32-bit and ARM-based boards while improving support for 8-bit AVR boards. Read about Marlin's decision to use a "Hardware Abstraction Layer" below.

Download earlier versions of Marlin on the [Releases page](https://github.com/MarlinFirmware/Marlin/releases).

## Example Configurations

Before building Marlin you'll need to configure it for your specific hardware. Your vendor should have already provided source code with configurations for the installed firmware, but if you ever decide to upgrade you'll need updated configuration files. Marlin users have contributed dozens of tested example configurations to get you started. Visit the [MarlinFirmware/Configurations](https://github.com/MarlinFirmware/Configurations) repository to find the right configuration for your hardware.

## Building Marlin 2.0

To build Marlin 2.0 you'll need [Arduino IDE 1.8.8 or newer](https://www.arduino.cc/en/main/software) or [PlatformIO](http://docs.platformio.org/en/latest/ide.html#platformio-ide). Detailed build and install instructions are posted at:

  - [Installing Marlin (Arduino)](http://marlinfw.org/docs/basics/install_arduino.html)
  - [Installing Marlin (VSCode)](http://marlinfw.org/docs/basics/install_platformio_vscode.html).

### Supported Platforms

  Platform|MCU|Example Boards
  --------|---|-------
  [Arduino AVR](https://www.arduino.cc/)|ATmega|RAMPS, Melzi, RAMBo
  [Teensy++ 2.0](http://www.microchip.com/wwwproducts/en/AT90USB1286)|AT90USB1286|Printrboard
  [Arduino Due](https://www.arduino.cc/en/Guide/ArduinoDue)|SAM3X8E|RAMPS-FD, RADDS, RAMPS4DUE
  [ESP32](https://github.com/espressif/arduino-esp32)|ESP32|FYSETC E4, E4d@BOX, MRR
  [LPC1768](http://www.nxp.com/products/microcontrollers-and-processors/arm-based-processors-and-mcus/lpc-cortex-m-mcus/lpc1700-cortex-m3/512kb-flash-64kb-sram-ethernet-usb-lqfp100-package:LPC1768FBD100)|ARM® Cortex-M3|MKS SBASE, Re-ARM, Selena Compact
  [LPC1769](https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/general-purpose-mcus/lpc1700-cortex-m3/512kb-flash-64kb-sram-ethernet-usb-lqfp100-package:LPC1769FBD100)|ARM® Cortex-M3|Smoothieboard, Azteeg X5 mini, TH3D EZBoard
  [STM32F103](https://www.st.com/en/microcontrollers-microprocessors/stm32f103.html)|ARM® Cortex-M3|Malyan M200, GTM32 Pro, MKS Robin, BTT SKR Mini
  [STM32F401](https://www.st.com/en/microcontrollers-microprocessors/stm32f401.html)|ARM® Cortex-M4|ARMED, Rumba32, SKR Pro, Lerdge, FYSETC S6
  [STM32F7x6](https://www.st.com/en/microcontrollers-microprocessors/stm32f7x6.html)|ARM® Cortex-M7|The Borg, RemRam V1
  [SAMD51P20A](https://www.adafruit.com/product/4064)|ARM® Cortex-M4|Adafruit Grand Central M4
  [Teensy 3.5](https://www.pjrc.com/store/teensy35.html)|ARM® Cortex-M4|
  [Teensy 3.6](https://www.pjrc.com/store/teensy36.html)|ARM® Cortex-M4|
  [Teensy 4.0](https://www.pjrc.com/store/teensy40.html)|ARM® Cortex-M7|
  [Teensy 4.1](https://www.pjrc.com/store/teensy41.html)|ARM® Cortex-M7|
  Linux Native|x86/ARM/etc.|Raspberry Pi

## Submitting Changes

- Submit **Bug Fixes** as Pull Requests to the ([bugfix-2.0.x](https://github.com/MarlinFirmware/Marlin/tree/bugfix-2.0.x)) branch.
- Follow the [Coding Standards](http://marlinfw.org/docs/development/coding_standards.html) to gain points with the maintainers.
- Please submit your questions and concerns to the [Issue Queue](https://github.com/MarlinFirmware/Marlin/issues).

## Marlin Support

The Issue Queue is reserved for Bug Reports and Feature Requests. To get help with configuration and troubleshooting, please use the following resources:

- [Marlin Documentation](http://marlinfw.org) - Official Marlin documentation
- [Marlin Discord](https://discord.gg/n5NJ59y) - Discuss issues with Marlin users and developers
- Facebook Group ["Marlin Firmware"](https://www.facebook.com/groups/1049718498464482/)
- RepRap.org [Marlin Forum](http://forums.reprap.org/list.php?415)
- [Tom's 3D Forums](https://forum.toms3d.org/)
- Facebook Group ["Marlin Firmware for 3D Printers"](https://www.facebook.com/groups/3Dtechtalk/)
- [Marlin Configuration](https://www.youtube.com/results?search_query=marlin+configuration) on YouTube

## Contributors

Marlin is constantly improving thanks to a huge number of contributors from all over the world bringing their specialties and talents. Huge thanks are due to [all the contributors](https://github.com/MarlinFirmware/Marlin/graphs/contributors) who regularly patch up bugs, help direct traffic, and basically keep Marlin from falling apart. Marlin's continued existence would not be possible without them.

## Administration

Regular users can open and close their own issues, but only the administrators can do project-related things like add labels, merge changes, set milestones, and kick trolls. The current Marlin admin team consists of:

 - Scott Lahteine [[@thinkyhead](https://github.com/thinkyhead)] - USA - Project Maintainer &nbsp; [![Donate](https://api.flattr.com/button/flattr-badge-large.png)](http://www.thinkyhead.com/donate-to-marlin)
 - Roxanne Neufeld [[@Roxy-3D](https://github.com/Roxy-3D)] - USA
 - Keith Bennett [[@thisiskeithb](https://github.com/thisiskeithb)] - USA
 - Victor Oliveira [[@rhapsodyv](https://github.com/rhapsodyv)] - Brazil
 - Chris Pepper [[@p3p](https://github.com/p3p)] - UK
 - Jason Smith [[@sjasonsmith](https://github.com/sjasonsmith)] - USA
 - Luu Lac [[@sjasonsmith](https://github.com/sjasonsmith)] - USA
 - Bob Kuhn [[@Bob-the-Kuhn](https://github.com/Bob-the-Kuhn)] - USA
 - Erik van der Zalm [[@ErikZalm](https://github.com/ErikZalm)] - Netherlands &nbsp; [![Flattr Erik](https://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=ErikZalm&url=https://github.com/MarlinFirmware/Marlin&title=Marlin&language=&tags=github&category=software)

## License

Marlin is published under the [GPL license](/LICENSE) because we believe in open development. The GPL comes with both rights and obligations. Whether you use Marlin firmware as the driver for your open or closed-source product, you must keep Marlin open, and you must provide your compatible Marlin source code to end users upon request. The most straightforward way to comply with the Marlin license is to make a fork of Marlin on Github, perform your modifications, and direct users to your modified fork.

While we can't prevent the use of this code in products (3D printers, CNC, etc.) that are closed source or crippled by a patent, we would prefer that you choose another firmware or, better yet, make your own.
